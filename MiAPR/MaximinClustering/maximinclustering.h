#ifndef MAXIMINCLUSTERING_H
#define MAXIMINCLUSTERING_H

#include <QVector>
#include <QPoint>
#include "cluster.h"

class MaximinClustering
{
public:
    MaximinClustering(QVector<QPoint>& points);
    ~MaximinClustering();
    void start();
    const QVector<Cluster*>& getClusters() const;

private:
    void setInitialCores();
    bool setNewCore();
    void bindPoints();
    int getDistance(const QPoint& fstPoint, const QPoint& sndPoint);

private:
    QVector<QPoint>*  points;
    QVector<Cluster*> clusters;
};

#endif // MAXIMINCLUSTERING_H
