#include "maximinclustering.h"
#include <qmath.h>

MaximinClustering::MaximinClustering(QVector<QPoint> &points)
{
    this->points = &points;
}

MaximinClustering::~MaximinClustering()
{
    foreach (Cluster* cluster, clusters) {
        delete cluster;
    }
}

void MaximinClustering::setInitialCores()
{
    int initPoint = 0;
    clusters.push_back(new Cluster(points->at(initPoint)));

    int pointCount = points->size();
    int tmpDistance = 0;
    int maxDistance = 0;

    for (int i = 1; i < pointCount; ++i) {
        tmpDistance = getDistance(clusters.at(0)->currentCore, points->at(i));

        if (tmpDistance > maxDistance) {
            maxDistance = tmpDistance;
            initPoint = i;
        }
    }
    clusters.push_back(new Cluster(points->at(initPoint)));
}

int MaximinClustering::getDistance(const QPoint& fstPoint, const QPoint& sndPoint)
{
    return sqrt(pow(fstPoint.x() - sndPoint.x(), 2) +
                pow(fstPoint.y() - sndPoint.y(), 2));
}

const QVector<Cluster*>& MaximinClustering::getClusters() const
{
    return clusters;
}

void MaximinClustering::bindPoints()
{
    foreach (Cluster* cluster, this->clusters) {
        cluster->clear();
    }
    int pointCount = this->points->size();
    int clusterCount = this->clusters.size();
    QPoint* points = this->points->data();
    Cluster** clusters = this->clusters.data();

    for (int i = 0; i < pointCount; ++i) {
        int min = getDistance(clusters[0]->currentCore, points[i]);
        Cluster* targetCluster = clusters[0];

        for(int j = 1; j < clusterCount; ++j) {
            int tmp = getDistance(clusters[j]->currentCore, points[i]);
            if (min > tmp) {
                min = tmp;
                targetCluster = clusters[j];
            }
        }
        targetCluster->points.push_back(points[i]);
    }
}

bool MaximinClustering::setNewCore()
{
    int tmpDistance = 0;
    int maxDistance = 0;
    QPoint targetPoint;

    foreach (Cluster* cluster, clusters) {
        foreach (QPoint point, cluster->getPoints()) {
            tmpDistance = getDistance(cluster->currentCore, point);

            if (tmpDistance > maxDistance) {
                maxDistance = tmpDistance;
                targetPoint = point;
            }
        }
    }
    int commonDistance = 0;

    foreach (Cluster* cluster, clusters) {
        commonDistance += getDistance(targetPoint, cluster->currentCore);
    }
    int halfOfAverageDistance = 0.5 * (commonDistance / clusters.size());

    if (maxDistance > halfOfAverageDistance) {
       clusters.push_back(new Cluster(targetPoint));
       return true;
    }
    return false;
}

void MaximinClustering::start()
{
    setInitialCores();

    forever {
        bindPoints();

        if (!setNewCore()) {
            return;
        }
    }
}
