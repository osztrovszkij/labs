#ifndef CLUSTER_H
#define CLUSTER_H

#include <QPoint>
#include <QVector>

class Cluster
{
public:
    Cluster(const QPoint& core);
    Cluster& setCore(const QPoint& core);
    QPoint getCore() const;
    Cluster& correctCore();
    bool isBestCore();
    Cluster& addPoint(QPoint& point);
    QVector<QPoint>& getPoints();

    int getSize() const;
    Cluster& clear();

//private:
    QPoint currentCore;
    QPoint lastCore;

    QVector<QPoint> points;
};

#endif // CLUSTER_H
