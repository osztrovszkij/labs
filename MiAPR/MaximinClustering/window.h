#ifndef WINDOW_H
#define WINDOW_H

#include <QWidget>
#include <QLabel>
#include <QSpinBox>

class RenderArea;

class Window : public QWidget
{
    Q_OBJECT
public:
    explicit Window(QWidget* parent = 0);

private slots:
    void pointsChanged();

private:
    RenderArea* renderArea;
    QLabel*     pointsLabel;
    QSpinBox*   pointsSpinBox;
};

#endif // WINDOW_H
