#include <QPainter>
#include "renderarea.h"
#include "maximinclustering.h"

RenderArea::RenderArea(QWidget* parent) : QWidget(parent)
{
    setPoints(1);
    penSize = 1;
}

void RenderArea::paintEvent(QPaintEvent* /* event */)
{
    QVector<QPoint> points;

    for (int i = 0; i < this->points; ++i) {
        points.push_back(QPoint(random() % width(),
                                random() % height()));
    }

    QPainter painter(this);
    painter.setBrush(QBrush(QColor(Qt::red)));
    painter.drawRect(rect());
    painter.setPen(QPen(Qt::black, penSize));

    foreach (QPoint point, points) {
        painter.drawPoint(point);
    }

    MaximinClustering clustering(points);
    clustering.start();

    foreach (Cluster* cluster, clustering.getClusters()) {
        //QColor color(random() % 256, random() % 256, random() % 256);
        //painter.setPen(QPen(color, penSize));

        foreach (QPoint point, cluster->getPoints()) {
            painter.drawLine(cluster->getCore(), point);
        }
    }
}

void RenderArea::setPoints(int points)
{
    this->points = points;
    update();
}
