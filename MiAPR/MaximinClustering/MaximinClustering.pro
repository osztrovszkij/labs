#-------------------------------------------------
#
# Project created by QtCreator 2016-02-15T22:26:23
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = MaximinClustering
TEMPLATE = app


SOURCES += main.cpp \
    cluster.cpp \
    renderarea.cpp \
    window.cpp \
    maximinclustering.cpp

HEADERS  += \
    cluster.h \
    renderarea.h \
    window.h \
    maximinclustering.h
