#include <QGridLayout>
#include "renderarea.h"
#include "window.h"

Window::Window(QWidget* parent) : QWidget(parent)
{
    renderArea = new RenderArea;

    pointsSpinBox   = new QSpinBox;
    pointsSpinBox->setMinimum(1);
    pointsSpinBox->setMaximum(999999);

    pointsLabel = new QLabel("&Points");
    pointsLabel->setBuddy(pointsSpinBox);

    connect(pointsSpinBox, SIGNAL(valueChanged(int)),
            this, SLOT(pointsChanged()));

    QGridLayout* mainLayout = new QGridLayout;
    mainLayout->setColumnStretch(3, 1);
    mainLayout->addWidget(renderArea, 0, 0, 1, 4);
    mainLayout->addWidget(pointsLabel, 2, 0);
    mainLayout->addWidget(pointsSpinBox, 2, 1);

    setLayout(mainLayout);
    setFixedSize(800, 600);
    setWindowTitle("Clustering");
}

void Window::pointsChanged()
{
    int points = pointsSpinBox->value();
    renderArea->setPoints(points);
}
