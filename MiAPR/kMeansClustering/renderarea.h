#ifndef RENDERAREA_H
#define RENDERAREA_H

#include <QWidget>

class RenderArea : public QWidget
{
    Q_OBJECT
public:
    explicit RenderArea(QWidget* parent = 0);

public slots:
    void setPoints(int points);
    void setClusters(int clusters);

protected:
    void paintEvent(QPaintEvent* /* event */);

private:
    int points;
    int clusters;
    int penSize;
};

#endif // RENDERAREA_H
