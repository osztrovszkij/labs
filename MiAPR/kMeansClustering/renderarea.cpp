#include <QPainter>
#include "renderarea.h"
#include "kmeansclustering.h"

RenderArea::RenderArea(QWidget* parent) : QWidget(parent)
{
    setPoints(1);
    setClusters(1);
    penSize = 4;
}

void RenderArea::paintEvent(QPaintEvent* /* event */)
{
    QVector<QPoint> points;

    for (int i = 0; i < this->points; ++i) {
        points.push_back(QPoint(random() % width(),
                                random() % height()));
    }

    QPainter painter(this);
    painter.setPen(QPen(Qt::black, penSize));

    foreach (QPoint point, points) {
        painter.drawPoint(point);
    }

    kMeansClustering clustering(points, this->clusters);
    clustering.start();

    foreach (Cluster* cluster, clustering.getClusters()) {
        QColor color(random() % 256, random() % 256, random() % 256);
        painter.setPen(QPen(color, penSize));

        foreach (QPoint point, cluster->getPoints()) {
            painter.drawPoint(point);
        }
    }
}

void RenderArea::setPoints(int points)
{
    this->points = points;
    update();
}

void RenderArea::setClusters(int clusters)
{
    this->clusters = clusters;
    update();
}
