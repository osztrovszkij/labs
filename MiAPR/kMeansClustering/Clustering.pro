#-------------------------------------------------
#
# Project created by QtCreator 2016-02-09T02:26:04
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Clustering
TEMPLATE = app


SOURCES += main.cpp \
    window.cpp \
    renderarea.cpp \
    cluster.cpp \
    kmeansclustering.cpp

HEADERS  += \
    window.h \
    renderarea.h \
    cluster.h \
    kmeansclustering.h
