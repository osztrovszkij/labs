#ifndef KMEANSCLUSTERING_H
#define KMEANSCLUSTERING_H

#include <QVector>
#include <QPoint>
#include "cluster.h"

class kMeansClustering
{
public:
    kMeansClustering(QVector<QPoint>& points, int clusters);
    ~kMeansClustering();
    void start();
    const QVector<Cluster*>& getClusters() const;

private:
    void initCores();
    void correctCores();
    void bindPoints();
    int getDistance(const QPoint& fstPoint, const QPoint& sndPoint);

private:
    QVector<QPoint>*  points;
    QVector<Cluster*> clusters;

};

#endif // KMEANSCLUSTERING_H
