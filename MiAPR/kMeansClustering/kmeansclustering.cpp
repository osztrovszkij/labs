#include "kmeansclustering.h"
#include <qmath.h>

kMeansClustering::kMeansClustering(QVector<QPoint>& points, int clusters)
{
    this->points = &points;

    for (int i = 0; i < clusters; ++i) {
        this->clusters.push_back(new Cluster());
    }
}

kMeansClustering::~kMeansClustering()
{
    foreach (Cluster* cluster, clusters) {
        delete cluster;
    }
}

void kMeansClustering::start()
{
    initCores();

    forever {
        int correctClusters = 0;
        bindPoints();
        correctCores();

        foreach (Cluster* cluster, clusters) {
            if (cluster->isBestCore())
                ++correctClusters;
        }
        if (correctClusters == clusters.size())
            return;
    }
}

const QVector<Cluster*>& kMeansClustering::getClusters() const
{
    return clusters;
}

void kMeansClustering::initCores()
{
    int delta = points->size() / clusters.size();
    int initPoint = 0;

    foreach (Cluster* cluster, clusters) {
        cluster->setCore(points->at(initPoint));
        initPoint += delta;
    }
}

void kMeansClustering::correctCores()
{
    foreach (Cluster* cluster, clusters) {
        cluster->correctCore();
    }
}

int kMeansClustering::getDistance(const QPoint& fstPoint, const QPoint& sndPoint)
{
    return sqrt(pow(fstPoint.x() - sndPoint.x(), 2) +
                pow(fstPoint.y() - sndPoint.y(), 2));
}

void kMeansClustering::bindPoints()
{
    foreach (Cluster* cluster, clusters) {
        cluster->clear();
    }
    int pointSize = this->points->size();
    int clusterSize = this->clusters.size();
    QPoint* points = this->points->data();
    Cluster** clusters = this->clusters.data();

    for (int i = 0; i < pointSize; ++i) {
        int min = getDistance(clusters[0]->currentCore, points[i]);
        Cluster* targetCluster = clusters[0];

        for(int j = 1; j < clusterSize; ++j) {
            int tmp = getDistance(clusters[j]->currentCore, points[i]);
            if (min > tmp) {
                min = tmp;
                targetCluster = clusters[j];
            }
        }
        targetCluster->points.push_back(points[i]);
    }

//    foreach (QPoint point, *points) {
//        int min = getDistance(clusters.at(0)->getCore(), point);
//        Cluster* targetCluster = clusters.at(0);

//        for (int i = 1; i < clusterSize; ++i) {
//            int tmp = getDistance(clusters.at(i)->getCore(), point);
//            if (min > tmp) {
//                min = tmp;
//                targetCluster = clusters.at(i);
//            }
//        }
//        targetCluster->addPoint(point);
//    }
}
