#include "cluster.h"

Cluster& Cluster::setCore(const QPoint& core)
{
    currentCore = core;
    return *this;
}

QPoint Cluster::getCore() const
{
    return currentCore;
}

Cluster& Cluster::correctCore()
{
    QPoint sum;

    foreach (QPoint point, points)
        sum += point;

    lastCore = currentCore;
    currentCore = sum / points.size();

    return *this;
}

bool Cluster::isBestCore()
{
    if (currentCore == lastCore)
        return true;
    else
        return false;
}

Cluster& Cluster::addPoint(QPoint& point)
{
    points.push_back(point);
    return *this;
}

QVector<QPoint>& Cluster::getPoints()
{
    return points;
}

int Cluster::getSize() const
{
    return points.size();
}

Cluster& Cluster::clear()
{
    points.clear();
    return *this;
}
