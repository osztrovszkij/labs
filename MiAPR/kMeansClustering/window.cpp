#include <QGridLayout>
#include "renderarea.h"
#include "window.h"

Window::Window(QWidget* parent) : QWidget(parent)
{
    renderArea = new RenderArea;

    pointsSpinBox   = new QSpinBox;
    pointsSpinBox->setMinimum(1);
    pointsSpinBox->setMaximum(999999);

    clustersSpinBox = new QSpinBox;
    clustersSpinBox->setMinimum(1);
    clustersSpinBox->setMaximum(999999);

    pointsLabel = new QLabel("&Points");
    pointsLabel->setBuddy(pointsSpinBox);

    clustersLabel = new QLabel("&Clusters");
    clustersLabel->setBuddy(clustersSpinBox);

    connect(pointsSpinBox, SIGNAL(valueChanged(int)),
            this, SLOT(pointsChanged()));
    connect(clustersSpinBox, SIGNAL(valueChanged(int)),
            this, SLOT(clustersChanged()));

    QGridLayout* mainLayout = new QGridLayout;
    mainLayout->setColumnStretch(3, 1);
    mainLayout->addWidget(renderArea, 0, 0, 1, 4);
    mainLayout->addWidget(pointsLabel, 2, 0);
    mainLayout->addWidget(pointsSpinBox, 2, 1);
    mainLayout->addWidget(clustersLabel, 3, 0);
    mainLayout->addWidget(clustersSpinBox, 3, 1);

    setLayout(mainLayout);
    setFixedSize(800, 600);
    setWindowTitle("Clustering");
}

void Window::pointsChanged()
{
    int points = pointsSpinBox->value();
    renderArea->setPoints(points);
}

void Window::clustersChanged()
{
    int clusters = clustersSpinBox->value();
    renderArea->setClusters(clusters);
}

