#ifndef WINDOW_H
#define WINDOW_H

#include <QWidget>
#include <QLabel>
#include <QSpinBox>

class RenderArea;

class Window : public QWidget
{
    Q_OBJECT
public:
    explicit Window(QWidget* parent = 0);

private slots:
    void pointsChanged();
    void clustersChanged();

private:
    RenderArea* renderArea;
    QLabel*     pointsLabel;
    QLabel*     clustersLabel;
    QSpinBox*   pointsSpinBox;
    QSpinBox*   clustersSpinBox;
};

#endif // WINDOW_H
